# Privacy Policy

Last updated: Feb. 24, 2024

## Introduction

We respects the privacy of our users. This Privacy Policy explains how we collect, use, disclose, and safeguard your information when you use our mobile application. Please read this privacy policy carefully. If you do not agree with the terms of this privacy policy, please do not access the application.

## Collection of Your Information

We may collect information about you in a variety of ways. The information we may collect via the Application depends on the content and materials you use, and includes:

### Personal Data

Demographic information, that you voluntarily give to us when choosing to participate in various activities related to the Application.

### Derivative Data

Information our servers automatically collect when you access the Application, such as your IP address, your browser type, your operating system, your access times, and the pages you have viewed directly before and after accessing the Application.

### [Other Information]

[Include other types of information your app may collect, such as location data, live data, etc.]

## Use of Your Information

Having accurate information about you permits us to provide you with a smooth, efficient, and customized experience. Specifically, we may use information collected about you via the Application to:

- [List specific uses of user data, such as improving the app, marketing, etc.]

## Policy for Children

We do not knowingly solicit information from or market to children under the age of 13. If you become aware of any data we have collected from children under age 13, please contact us using the contact information provided below.

## Contact Us

If you have questions or comments about this Privacy Policy, please contact us.